Gem::Specification.new do |spec|
  spec.name = 'rpsl'
  spec.version = '1.0.2'
  spec.summary = 'Library which provides functions for reading and writing RPSL.'
  spec.description = <<~DESCR.chomp
    Library which provides functions for reading and writing Routing Policy Specification Language (RPSL, RFC2622).
  DESCR
  spec.authors = ['Marek Küthe']
  spec.email = 'm.k@mk16.de'

  spec.files = %w[lib/rpsl.rb]
  spec.extra_rdoc_files = %w[LICENSE README.md]

  spec.homepage = 'https://codeberg.org/mark22k/rpsl'
  spec.license = 'WTFPL'

  spec.metadata = { 'source_code_uri' => 'https://codeberg.org/mark22k/rpsl',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/rpsl/issues',
                    'rubygems_mfa_required' => 'true' }

  spec.required_ruby_version = '>= 3.1'
end
