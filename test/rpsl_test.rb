require 'minitest/autorun'

class TestRPSL < Minitest::Test
  require_relative '../lib/rpsl'

  # rubocop:disable Layout/LineContinuationLeadingSpace
  # rubocop:disable Style/StringLiterals
  # rubocop:disable Layout/HashAlignment
  RPSL_STRING1 = "route6:             fd42:4242:2601:ffff::/64\n" \
                 "descr:              This is an ROA test object\n" \
                 "max-length:         112\n" \
                 "remarks:            \n" \
                 "                    This object is designed to test ROA scripts\n" \
                 "                    in a number of ways.\n" \
                 "+\n" \
                 "                    The expected output for this route should be:\n" \
                 "                    roa fd42:4242:2601:ffff::/64 max 64 as 0;\n" \
                 "+\n" \
                 "                    This is not a blank line\n" \
                 "                    + ----- +\n" \
                 "+\n" \
                 "                    The first test is to include a number of syntax\n" \
                 "                    corner cases, designed to trip up parsers\n" \
                 "+\n" \
                 "                    The first max-length line should be clamped\n" \
                 "                    to the maximums defined in filter6.txt\n" \
                 "+\n" \
                 "                    The second max-length is part of the remark\n" \
                 "                    and should be ignored.\n" \
                 "+\n" \
                 "                    max-length: 48\n" \
                 "origin:             AS0\n" \
                 "mnt-by:             BURBLE-MNT\n" \
                 "source:             DN42\n".freeze
  RPSL_OBJECT1 = { "route6"     => "fd42:4242:2601:ffff::/64",
                   "descr"      => "This is an ROA test object",
                   "max-length" => "112",
                   "remarks"    =>
                                   "\n" \
                                   "This object is designed to test ROA scripts\n" \
                                   "in a number of ways.\n" \
                                   "\n" \
                                   "The expected output for this route should be:\n" \
                                   "roa fd42:4242:2601:ffff::/64 max 64 as 0;\n" \
                                   "\n" \
                                   "This is not a blank line\n" \
                                   "+ ----- +\n" \
                                   "\n" \
                                   "The first test is to include a number of syntax\n" \
                                   "corner cases, designed to trip up parsers\n" \
                                   "\n" \
                                   "The first max-length line should be clamped\n" \
                                   "to the maximums defined in filter6.txt\n" \
                                   "\n" \
                                   "The second max-length is part of the remark\n" \
                                   "and should be ignored.\n" \
                                   "\n" \
                                   "max-length: 48",
                   "origin"     => "AS0",
                   "mnt-by"     => "BURBLE-MNT",
                   "source"     => "DN42" }.freeze
  # rubocop:enable Layout/HashAlignment
  # rubocop:enable Style/StringLiterals
  # rubocop:enable Layout/LineContinuationLeadingSpace

  def test_obj_to_rpsl
    assert_equal RPSL_OBJECT1, RPSL.load(RPSL_STRING1).transform_values!(&:chomp)
  end

  def test_rpsl_to_obj
    assert_equal RPSL_STRING1, RPSL.dump(RPSL_OBJECT1, 20)
  end

  def test_consistent
    (17..40).each do |index|
      str = RPSL.dump(RPSL_OBJECT1, index)
      new_str = RPSL.dump(RPSL.load(str), index)

      assert_equal(str, new_str)
    end
  end
end
